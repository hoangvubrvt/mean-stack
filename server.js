const http = require("http");
const app = require("./backend/app");

const PORT = 3000;
app.set("port", PORT)
const server = http.createServer(app);
server.listen(PORT, () => {
    console.log(`Backend is started with ${PORT} port`);
});