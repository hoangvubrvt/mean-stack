const express = require("express");
const Post = require("../models/post");
const multer = require("multer");
const router = express.Router();

const MINE_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'
}

// Uploading image middleware: https://github.com/expressjs/multer
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const isValid = MINE_TYPE_MAP[file.mimetype];
        let error = new Error('Invalid mime type');
        if(isValid) {
            error = null;
        }
        cb(error, "backend/images")
    },
    filename: (req, file, cb) => {
        const name = file.originalname.toLowerCase().split(" ").join("-");
        const ext = MINE_TYPE_MAP[file.mimetype];
        cb(null, `${name}-${Date.now()}.${ext}`);
    }
})

router.get("", (request, response, next) => {
    return Post.find().then( documents => {
        console.log(documents);
        return response.status(200).json({
            message: "This is response from server",
            posts: documents
        })
    }); 
});

router.post("", multer({storage: storage}).single("image"), (req, res, next) => {
    let post = new Post({
        title: req.body.title,
        content: req.body.content,
        imagePath: `${req.protocol}://${req.get("host")}/images/${req.file.filename}`
    });
    post.save().then(createdPost => {
        console.log(createdPost)
        return res.status(201).json({
            message: "Post added successfully",
            post: {
                id: createdPost._id,
                title: createdPost.title,
                content: createdPost.content,
                imagePath: createdPost.imagePath
            }
        })
    })
})

router.delete("/:id", (req, res, next) => {
    postId = req.params.id;
    Post.deleteOne({
        _id: postId
    }).then(result => {
        console.log(result);
        res.status(200).json({
            message: "Post deleted"
        })
    })
})

router.put("/:id", multer({storage: storage}).single("image"), (req, res, next) => {
    let imagePath = req.body.imagePath
    if(req.file) {
        imagePath = `${req.protocol}://${req.get("host")}/images/${req.file.filename}`
    }
    const post = new Post({
        _id: req.params.id,
        title: req.body.title,
        content: req.body.content,
        imagePath: imagePath
    })
    Post.updateOne({
        _id: req.params.id
    }, post).then(result => {
        console.log(result);
        return res.status(200).json({
            message: "Update successful!"
        })
    })
})

router.get("/:id", (req, res, next) => {
    Post.findById(req.params.id).then(post => {
        if(post) {
            res.status(200).json(post)
        } else {
            res.status(404).json({
                message: "Post not found"
            })
        }
    })
}) 

module.exports = router