const path = require("path")
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const postRouter = require("./routes/posts")
mongoose.connect("mongodb://localhost:27017/mean-stack").then( () => {
    console.log("Connected to Database!");
}).catch(() => {
    console.log("Connection failed!");
})

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.use("/images", express.static(path.join("backend/images")))
app.use("/api/posts", postRouter)

module.exports = app;