import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  posts:Post[] = []
  isLoading = false;
  
  constructor(private postService:PostService) { }

  ngOnInit() {
    this.isLoading = true
    this.postService.getPosts()
    this.postService.getPostUpdatedListener().subscribe(posts => {
      this.posts = posts;
      this.isLoading = false;
    })
  }

  onDelete(id:String) {
    this.postService.deletePost(id);
  }
}
