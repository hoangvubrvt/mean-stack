import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
    providedIn: "root"
})
export class PostService {
    private posts:Post[] = [];
    private postsUpdated = new Subject<Post[]>();

    constructor(private http:HttpClient, private router: Router) {}

    getPosts() {
        this.http.get<{message:String, posts: any}>("http://localhost:3000/api/posts").pipe(
            map((response) => {
                return response.posts.map(item => {
                    return {
                        title: item.title,
                        content: item.content,
                        id: item._id,
                        imagePath: item.imagePath
                    }
                });
            })
        ).subscribe((posts) => {
            this.posts = posts
            this.postsUpdated.next(posts)
        })
    }

    getPost(id:String) {
        return this.http.get<{ post:any }>(`http://localhost:3000/api/posts/${id}`);
    }

    getPostUpdatedListener(){
        return this.postsUpdated.asObservable();
    }

    updatePost(id:string, title:string, content:string, image:File | string) {
        let post: Post | FormData;
        if(typeof image === "object") {
            post = new FormData();
            post.append("title", title);
            post.append("content", content);
            post.append("image", image, title);
            post.append("id", id);
        } else {
            post = {
                id: id,
                title: title,
                content: content,
                imagePath: image
            }
        }

        this.http.put(`http://localhost:3000/api/posts/${id}`, post).subscribe(response => {
            console.log(response);
            const updatedPosts = [...this.posts];
            const oldPostIndex = updatedPosts.findIndex(p => p.id === id); 
            this.router.navigate(["/"]);
        })
    }

    addPost(title:string, content:string, image: File) {
        const postData = new FormData();
        postData.append("title", title);
        postData.append("content", content);
        postData.append("image", image, title);

        this.http.post<{message:string, post:Post}>("http://localhost:3000/api/posts", postData).subscribe(response => {
            const post: Post = response.post;
            this.posts.push(post);
            this.postsUpdated.next([...this.posts])
            this.router.navigate(["/"]);
        })
    }

    deletePost(id:String) {
        this.http.delete(`http://localhost:3000/api/posts/${id}`).subscribe(response => {
            let updatedPosts = this.posts.filter(post => post.id !== id);
            this.posts = updatedPosts;
            console.log(this.posts)
            this.postsUpdated.next([...this.posts]);
        })
    }
}