import { Component, EventEmitter, Output, OnInit } from "@angular/core";
import { Post } from '../post.model';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { PostService } from '../post.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { mineType } from './mine-type.validator';

@Component({
    templateUrl: "./post-create.component.html",
    styleUrls: ["./post-create.component.css"],
    selector: "app-post-create"
})
export class PostCreateComponent implements OnInit{
    newPost = "NO CONTENT"
    enteredTitle = ""
    enteredContent = ""
    isLoading = false;
    imagePreview;
    private mode = "create";
    private postId: string;
    private post:Post;

    form:FormGroup;

    constructor(private postService: PostService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.form = new FormGroup({
            title: new FormControl(null, {
                validators: [Validators.required, Validators.minLength(3)]
            }),
            content: new FormControl(null, {validators: [Validators.required]}),
            image: new FormControl(null, {
                validators: [Validators.required],
                asyncValidators: [mineType]})
        });

        this.isLoading = true
        this.route.paramMap.subscribe((paramMap:ParamMap) => {
            if(paramMap.has("postId")) {
                this.mode = "edit";
                this.postId = paramMap.get("postId");
                this.postService.getPost(this.postId).subscribe( (postData:any) => {
                    this.post = {
                        id: postData._id,
                        title: postData.title,
                        content: postData.content,
                        imagePath: postData.imagePath
                    }
                    this.form.setValue({
                        title: this.post.title,
                        content: this.post.content,
                        image: this.post.imagePath
                    })
                    this.isLoading = false;
                });
            } else {
                this.mode = "create";
                this.postId = null;
                this.isLoading = false
            }
        })
    }

    onSavePost() {
        if(this.form.invalid) {
            return;
        }
        this.isLoading = true

        if(this.mode === "create") {
            this.postService.addPost(this.form.value.title, this.form.value.content, this.form.value.image);
        } else {
            this.postService.updatePost(this.postId, this.form.value.title, this.form.value.content, this.form.value.image);
        }
        this.form.reset()
    }

    onImagePicked(event:Event){
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({image: file});
        this.form.get("image").updateValueAndValidity();

        const reader = new FileReader();
        reader.onload = () => {
            this.imagePreview = reader.result;
        }
        reader.readAsDataURL(file);
    }
}